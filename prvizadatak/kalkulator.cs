using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prvianaliza
{
    public partial class Kalkulator : Form
    {
        double x, y, rez;

        public Kalkulator()
        {
            InitializeComponent();
        }

        private void Btn_zbroj_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tb_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(tb_operand2.Text, out y))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rez = x + y;
        }

        private void Btn_razlika_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tb_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(tb_operand2.Text, out y))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rez = x - y;
        }

        private void Btn_umnozak_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tb_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(tb_operand2.Text, out y))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rez = x * y;
        }

       
        private void Btn_podijeli_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tb_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(tb_operand2.Text, out y) || y == 0)
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rez = x / y;
        }

        private void Btn_sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tb_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rez = Math.Sin(x);
        }

        private void Btn_cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tb_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rez = Math.Cos(x);
        }

        private void Btn_log_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tb_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rez = Math.Log(x);
        }

        private void Btn_sqrt_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tb_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rez = Math.Sqrt(x);
        }

        private void Btn_exp_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tb_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rez = Math.Exp(x);
        }

	private void btn_Calculate_Click(object sender, EventArgs e)
	{
		tb_Rez.Text = rez.ToString();
		tb_Rez.Show();
		tb1.Clear();
		tb2.Clear();
	}

    }
    }